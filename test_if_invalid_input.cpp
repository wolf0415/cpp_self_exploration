
#include <iostream>
#include <vector>
#include <string>

using namespace std;
// https://stackoverflow.com/questions/5131647/why-would-we-call-cin-clear-and-cin-ignore-after-reading-input#:~:text=The%20cin.,not%20cause%20another%20parse%20failure).
// https://flylib.com/books/en/2.253.1/stream_error_states.html



int main()
{
	int n;
	cout << "Please enter a number: " ;
	cin >> n;
	if (cin.good()) { // return true if the input fail
		cout << "It's an integer" << endl;  
		cout << n << endl;
	}
	else {
		cout << "good luck" << endl; 
	}		
	cin.clear();
	cin.ignore(1000, '\n');
	cout << n << endl; 

/*
	string name;
	cout << "Give me your name and surname:" << endl;
	cin >> name;
	int age;
	cout << "Give me your age:" << endl;
	cin >> age;
	cout << cin.rdstate() << endl;
	cin.clear();
	cout << cin.rdstate() << endl;*/

	//std::cin.good(); // return true if the correct type or else
	//std::cin.fail(); // return true if incorrect type inputted
	//std::cin.clear(); // clear the error flag (set `cin` to goodbit) 
	//std::cin.ignore(); // removing the input contents that could caused the read failure. 
	//cin.rdstate(); // return the state of the variable 
	//// Often with this one
	//// mean either next 1000 characters or the characters until '\n' shall be ignored, whichever comes first.
	//cin.ignore(1000, '\n'); 
	//cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	return 0;
}
