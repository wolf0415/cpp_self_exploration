#include <iostream>
#include <string>
using namespace std;

class People {
public:

	People() : name(" "), age(0) {};

	void setName(std::string n) {
		name = n;
	}

	std::string getName() {
		return this->name;
	}

	void setAge(int a) {
		age = a;
	}
	int getAge() {
		return this->age;
	}

private:
	int age;
	std::string name;

};

int main() {
	std::string studentName;
	int studentAge;
	int studentAge1;
	People student;
	std::cout << "Please enter the name of the student: ";
	//std::cin >> studentName;
	std::getline(std::cin, studentName);

	std::cout << "Please enter the age of the student: ";
	std::cin >> studentAge;
	std::cout << "The type of studentAge that used cin is: " << typeid(studentAge).name() << std::endl;

	std::cout << "Please enter the age of the student: ";
	// Since `getline()` is a function from <string> library, it will not allow us to accept integer as an input
	//std::getline(std::cin, studentAge1); // it will not work
	std::cout << "The type of studentAge that used cin is: " << typeid(studentAge).name() << std::endl;

	student.setName(studentName);
	std::cout << "There is a student called: " << student.getName() << std::endl;
	student.setAge(studentAge);
	std::cout << "The age of the student [" << student.getName() << "] is: " << student.getAge() << std::endl;


	return 0;
}